FROM node:10.7

RUN npm install -g mocha && npm cache verify

COPY ./wait-for-it.sh /wait-for-it.sh

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

RUN mkdir -p /opt/app && chown -R node:node /opt/app

ENV NODE_PATH /usr/local/lib/node_modules

USER node

WORKDIR /opt/app
COPY . /opt/app
RUN npm install && npm cache clean --force

ENV PATH /opt/app/node_modules/.bin:$PATH

CMD ["npm", "start"]
