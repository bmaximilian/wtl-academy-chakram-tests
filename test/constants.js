/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

module.exports = {
    baseUrl: process.env.BASE_URL || 'http://api:80/api',
    headerName: process.env.AUTH_HEADER_NAME || 'token',
};
