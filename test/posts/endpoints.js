/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

module.exports = {
    list: '/posts',
    add: '/posts',
    get: '/posts/{id}',
    edit: '/posts/{id}',
    destroy: '/posts/{id}',
};
