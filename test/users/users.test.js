/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const {
    get,
    post,
    put,
    expect,
} = require('chakram');
const chakram = require('chakram');
const { describe, it } = require('mocha');
const {
    getApiUrl,
    checkIfUser,
    checkIfResponse,
} = require('../../utils');
const {
    list,
    add,
    destroy,
    edit,
    get: getUser,
} = require('./endpoints');

const withLogs = false;
const log = withLogs ? console.log : () => {};

describe('Users', () => {
    let initialUsersLength = 0;
    const addedUser = {
        name: 'Uwe Kowalsky',
        email: 'uko@local.wtl.de',
    };
    // let addedUserToken = '';
    let addedUserId = 0;

    describe('Users: list', () => {
        it('Should show all existing users', async () => {
            const response = await get(getApiUrl(list));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfResponse(response).hasCollectionWithName('users');

            initialUsersLength = response.body.users.length;
            log('Initial length of users: ', initialUsersLength);

            if (initialUsersLength > 0) {
                checkIfUser(response.body.users[0]);
            }
        });
    });

    describe('Users: add', () => {
        it('Should add a user', async () => {
            const response = await post(getApiUrl(add), addedUser);

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfUser(response.body).hasToken();

            expect(response.body.name).to.equal(addedUser.name);
            expect(response.body.email).to.equal(addedUser.email);

            addedUserId = response.body.id;
            // addedUserToken = response.body.token;

            log('Added user with id: ', addedUserId);
        });

        it('Should show all existing users containing the recently added user', async () => {
            const response = await get(getApiUrl(list));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfResponse(response).hasCollectionWithName('users');

            expect(initialUsersLength + 1).to.equal(response.body.users.length);

            const recentUser = response.body.users[response.body.users.length - 1];
            checkIfUser(recentUser);
            expect(recentUser.name).to.equal(addedUser.name);
            expect(recentUser.email).to.equal(addedUser.email);
            expect(recentUser.id).to.equal(addedUserId);
        });
    });

    describe('Users: get', () => {
        it('Should get the recently added user', async () => {
            const response = await get(getApiUrl(getUser, { id: addedUserId }));

            checkIfResponse(response).isSuccessfulJSONResponse();

            checkIfUser(response.body);

            expect(response.body.name).to.equal(addedUser.name);
            expect(response.body.email).to.equal(addedUser.email);
            expect(response.body.id).to.equal(addedUserId);
        });
    });

    describe('Users: edit', () => {
        it('Should edit the recently added user', async () => {
            addedUser.name = 'Uwe Müller';
            addedUser.email = 'umu@local.wtl.de';
            const response = await put(getApiUrl(edit, { id: addedUserId }), addedUser);

            checkIfResponse(response).isSuccessfulJSONResponse();

            checkIfUser(response.body);

            expect(response.body.name).to.equal(addedUser.name);
            expect(response.body.email).to.equal(addedUser.email);
            expect(response.body.id).to.equal(addedUserId);
        });

        it('Should get the recently edited user', async () => {
            const response = await get(getApiUrl(getUser, { id: addedUserId }));

            checkIfResponse(response).isSuccessfulJSONResponse();

            checkIfUser(response.body);

            expect(response.body.name).to.equal(addedUser.name);
            expect(response.body.email).to.equal(addedUser.email);
            expect(response.body.id).to.equal(addedUserId);
        });
    });

    describe('Users: delete', () => {
        it('Should delete the added user', async () => {
            const response = await chakram.delete(getApiUrl(destroy, { id: addedUserId }));

            checkIfResponse(response).isSuccessfulJSONResponse();

            log('deleted user with id: ', addedUserId);
        });

        it('Should not find the deleted user', async () => {
            const response = await get(getApiUrl(getUser, { id: addedUserId }));

            checkIfResponse(response).isNotFoundResponse();
        });
    });
});
