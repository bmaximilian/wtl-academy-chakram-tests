/**
 * Created on 03.11.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const {
    get,
    post,
    put,
    expect,
} = require('chakram');
const chakram = require('chakram');
const { describe, it } = require('mocha');
const {
    getApiUrl,
    checkIfPost,
    checkIfResponse,
    checkIfUser,
    checkIfComment,
} = require('../../utils');
const { headerName } = require('../constants');
const {
    list,
    add,
    destroy,
    edit,
    get: getComment,
} = require('./endpoints');
const { add: addUser, destroy: destroyUser } = require('../users/endpoints');
const { add: addPost, destroy: destroyPost } = require('../posts/endpoints');

const withLogs = false;
const log = withLogs ? console.log : () => {};

const getDefaultOptions = token => ({
    headers: {
        [headerName]: token,
    },
});

describe('Comments', () => {
    const addedUser = {
        name: 'Comments Testuser',
        email: 'ctu@local.wtl.de',
    };
    const addedPost = {
        message: 'Das ist ein Test für Comments!',
    };
    const addedComment = {
        message: 'Testkommentar',
    };
    let addedUserToken = '';
    let addedUserId = 0;
    let addedPostId = 0;
    let addedCommentId = 0;
    let initialCommentsLength = 0;

    describe('Add User and Post', () => {
        it('Should add a user', async () => {
            const response = await post(getApiUrl(addUser), addedUser);

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfUser(response.body).hasToken();

            expect(response.body.name).to.equal(addedUser.name);
            expect(response.body.email).to.equal(addedUser.email);

            addedUserId = response.body.id;
            addedUserToken = response.body.token;

            log('Added user with id: ', addedUserId);
        });

        it('Should add a post', async () => {
            const response = await post(getApiUrl(addPost), addedPost, getDefaultOptions(addedUserToken));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfPost(response.body).andIncludesAuthor();

            expect(response.body.message).to.equal(addedPost.message);

            addedPostId = response.body.id;

            log('Added post with id: ', addedUserId);
        });
    });

    describe('Comments: list', () => {
        it('Should list the comments of the added post', async () => {
            const response = await get(getApiUrl(list, { postId: addedPostId }));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfResponse(response).hasCollectionWithName('comments');

            initialCommentsLength = response.body.comments.length;
            log('Initial length of comments: ', initialCommentsLength);

            expect(initialCommentsLength).to.equal(0);
        });
    });

    describe('Comments: add', () => {
        it('Should add a comment to the post', async () => {
            const response = await post(
                getApiUrl(add, { postId: addedPostId }),
                addedComment,
                getDefaultOptions(addedUserToken),
            );

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfComment(response.body);

            expect(response.body.message).to.equal(addedComment.message);
            addedCommentId = response.body.id;
        });

        it('Should list the comments of the added post with the added comment', async () => {
            const response = await get(getApiUrl(list, { postId: addedPostId }));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfResponse(response).hasCollectionWithName('comments');

            expect(initialCommentsLength + 1).to.equal(response.body.comments.length);
        });
    });

    describe('Comments: get', () => {
        it('Should get the added comment', async () => {
            const response = await get(
                getApiUrl(getComment, { postId: addedPostId, id: addedCommentId }),
            );

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfComment(response.body).andIncludesAuthor();

            expect(response.body.message).to.equal(addedComment.message);
            expect(response.body.id).to.equal(addedCommentId);
        });
    });

    describe('Comments: edit', () => {
        it('Should edit the added comment', async () => {
            addedComment.message = 'The message is edited.';

            const response = await put(
                getApiUrl(edit, { postId: addedPostId, id: addedCommentId }),
                addedComment,
                getDefaultOptions(addedUserToken),
            );

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfComment(response.body);

            expect(response.body.message).to.equal(addedComment.message);
            expect(response.body.id).to.equal(addedCommentId);
        });
    });

    describe('Comments: delete', () => {
        it('Should delete a comment from the post', async () => {
            const response = await chakram.delete(
                getApiUrl(destroy, { postId: addedPostId, id: addedCommentId }),
                {},
                getDefaultOptions(addedUserToken),
            );

            checkIfResponse(response).isSuccessfulJSONResponse();
        });

        it('Should not find the deleted comment', async () => {
            const response = await get(
                getApiUrl(getComment, { postId: addedPostId, id: addedCommentId }),
                {},
                getDefaultOptions(addedUserToken),
            );

            checkIfResponse(response).isNotFoundResponse();
        });
    });

    describe('Delete User and Post', () => {
        it('Should delete the post', async () => {
            const response = await chakram.delete(
                getApiUrl(destroyPost, { id: addedPostId }),
                {},
                getDefaultOptions(addedUserToken),
            );

            checkIfResponse(response).isSuccessfulJSONResponse();

            log('deleted post with id: ', addedPostId);
        });

        it('Should delete the user', async () => {
            const response = await chakram.delete(getApiUrl(destroyUser, { id: addedUserId }));

            checkIfResponse(response).isSuccessfulJSONResponse();

            log('deleted user with id: ', addedUserId);
        });
    });
});
