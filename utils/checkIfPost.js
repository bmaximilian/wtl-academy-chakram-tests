/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const { expect } = require('chakram');
const checkIfUser = require('./checkIfUser');

/**
 * Tests if the passed object is a post
 *
 * @param {object} object
 * @return {object}
 * @throws
 */
function checkIfPost(object) {
    expect(object).to.be.an('object');
    expect(object).to.have.property('id');
    expect(object).to.have.property('message');
    expect(object.id).to.be.at.least(1);
    expect(object.message).to.be.a('string');

    return {
        andIncludesAuthor() {
            expect(object).to.have.property('author');
            expect(object.author).to.be.an('object');
            checkIfUser(object.author);

            return expect(object);
        },
        andIncludesComments() {
            expect(object).to.have.property('comments');
            expect(object.comments).to.be.an('array');

            return expect(object);
        },
        ...expect(object),
    };
}

module.exports = checkIfPost;
