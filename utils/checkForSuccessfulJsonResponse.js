/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const { expect } = require('chakram');

/**
 * Tests if the passed response is a successful JSON response
 *
 * @param {object} response
 * @return {void}
 * @throws
 */
function checkForSuccessfulJsonResponse(response) {
    expect(response.response.statusCode).to.be.below(400);
    expect(response.response.statusCode).to.be.at.least(200);
    // expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
    // expect(response).not.to.be.encoded.with.gzip;
}

module.exports = checkForSuccessfulJsonResponse;
